<?php
function tarifParkir($masuk, $keluar)
{
    $waktuMasuk = new DateTime($masuk);
    $waktuKeluar = new DateTime($keluar);

    $interval = $waktuMasuk->diff($waktuKeluar);
    $jam = $interval->h + ($interval->days * 24);
    $durasi = $interval->s + ($interval->i * 60) + (($interval->h + ($interval->days * 24)) * 3600);

    $ketentuanTarif1 = $durasi <= (8 * 3600);
    $ketentuanTarif2 = (8 * 3600) < $durasi && $durasi <= (24 * 3600);
    $ketentuanTarif3 = (24 * 3600) < $durasi;

    switch (true) {
        case $ketentuanTarif1:
            $tarif = $jam * 1000;
            break;
        case $ketentuanTarif2:
            $tarif = 8000;
            break;
        case $ketentuanTarif3:
            $tarif = 15000 + (($jam - 24) * 1000);
            break;
        default:
            $tarif = 0;
            break;
    }

    $tarifParkir = 'Rp.' . number_format($tarif, 2);
    $hasil = array(
        'masuk' => $masuk,
        'keluar' => $keluar,
        'tarif' => $tarifParkir
    );

    return $hasil;
}

$output = tarifParkir('2019/01/27 05:00:01', '2019/01/27 17:45:03');
echo ("Tanggal " . $output['masuk'] . " sampai dengan tanggal " . $output['keluar'] . " tarif parkirnya adalah " . $output['tarif'] . "\n");

$output2 = tarifParkir('2019/01/27 07:03:59', '2019/01/27 15:30:06');
echo ("Tanggal " . $output2['masuk'] . " sampai dengan tanggal " . $output2['keluar'] . " tarif parkirnya adalah " . $output2['tarif'] . "\n");

$output3 = tarifParkir('2019/01/27 07:05:00', '2019/01/28 00:20:21');
echo ("Tanggal " . $output3['masuk'] . " sampai dengan tanggal " . $output3['keluar'] . " tarif parkirnya adalah " . $output3['tarif'] . "\n");

$output4 = tarifParkir('2019/01/27 11:14:23', '2019/01/27 13:20:00');
echo ("Tanggal " . $output4['masuk'] . " sampai dengan tanggal " . $output4['keluar'] . " tarif parkirnya adalah " . $output4['tarif']);
