<?php
function sensorNama($nama)
{
    $countNama = strlen($nama);
    $namaArr = explode(' ', $nama);

    foreach ($namaArr as $arr) {
        $x = str_split($arr);
        for ($i = 0; $i < count($x); $i++) {
            if ($i != 0 && $i != strlen($arr) - 1) {
                $x[$i] = "*";
            }
        }
        $y = implode('', $x);
        echo (" " . $y);
    }
}

$input = "Susilo Bambang Yudhoyono";
echo ("Input\n" . $input . "\n\n");

echo ("Output\n");
sensorNama($input);
