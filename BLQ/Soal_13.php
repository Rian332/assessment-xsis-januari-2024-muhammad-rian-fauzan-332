<?php
function sudutJam($waktu)
{
    $waktuArr = explode(':', $waktu);
    $jam = $waktuArr[0];
    $menit = $waktuArr[1];

    $konversiSudutJam = 360 / (12 * 60);
    $konversiSudutMenit = 360 / 60;

    $sudutJam = $konversiSudutJam * (60 * $jam + $menit);
    $sudutMenit = $konversiSudutMenit * $menit;
    $sudutWaktu = abs($sudutJam - $sudutMenit);

    return $sudutWaktu;
}

$input = "3:00";
$input2 = "5:30";
$input3 = "2:20";
$output = sudutJam($input);
$output2 = sudutJam($input2);
$output3 = sudutJam($input3);
echo ("Jam " . $input . " -> " . $output . "\n");
echo ("Jam " . $input2 . " -> " . $output2 . "\n");
echo ("Jam " . $input3 . " -> " . $output3);
