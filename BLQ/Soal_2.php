<?php
function dendaPerpus($hariPinjam, $hariKembalikan)
{
    $buku = array(
        'A' => 14,
        'B' => 3,
        'C' => 7,
        'D' => 7
    );

    $dendaPerHari = 100;
    $tanggalPinjam = new DateTime($hariPinjam);
    $tanggalKembalikan = new DateTime($hariKembalikan);
    $interval = $tanggalPinjam->diff($tanggalKembalikan);
    $durasi = $interval->days;
    $dendaBuku = array();

    foreach ($buku as $key => $value) {
        $waktuDenda = $durasi - $value;

        if ($waktuDenda > 0) {
            $denda = $waktuDenda * $dendaPerHari;
        } else {
            $denda = 0;
        }

        $dendaBuku[$key] = $denda;
    };

    echo "Pada tanggal $hariPinjam - $hariKembalikan buku-buku yang terkena denda adalah \n";
    foreach ($dendaBuku as $key => $value) {
        if ($value > 0) {
            $value = 'Rp.' . number_format($value);
            echo "Buku $key terkena denda $value";
        } else {
            echo "buku $key tidak terkena denda";
        }
        echo "\n";
    }
}

echo ("Jawaban pertanyaan A.\n");
echo (dendaPerpus('28-02-2016', '07-03-2016') . "\n");

echo ("jawaban pertanyaan B. \n");
echo (dendaPerpus('29-4-2018', '30-5-2018'));
