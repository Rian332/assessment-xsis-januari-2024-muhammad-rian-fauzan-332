<?php
function tengahBintang($string)
{
    $revString = strrev($string);
    $lenString = strlen($string);
    $arrString = str_split($revString);

    $x = "";
    if ($lenString % 2 == 0) {
        for ($i = 1; $i <= $lenString / 2; $i++) {
            $x .= "*";
        }
    } else {
        for ($i = 1; $i <= ($lenString - 1) / 2; $i++) {
            $x .= "*";
        }
    }

    foreach ($arrString as $newString) {
        echo ($x . $newString . $x . "\n");
    }
}

$input = "Afrika";
echo ("Input: " . $input . "\n");
echo ("Output:\n");
tengahBintang($input);

echo "\n";

$input = "Jeruk";
echo ("Input: " . $input . "\n");
echo ("Output:\n");
tengahBintang($input);
