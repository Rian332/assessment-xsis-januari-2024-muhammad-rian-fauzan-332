<?php
function palindrome($string)
{
    $strLow = strtolower($string);
    $strArr = str_split($strLow);

    for ($i = 0; $i < count($strArr); $i++) {
        if ($strArr[$i] != $strArr[count($strArr) - ($i + 1)]) {
            return false;
        }
    }
    return true;
}
$input = "Malam";
if (palindrome($input)) {
    echo ("Kata " . $input . " adalah kata palindrome");
} else {
    echo ("Kata " . $input . " bukan kata palindrome");
}
