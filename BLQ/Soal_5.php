<?php
function fibonacciN($n)
{
    $fibonacci = array(1, 1);

    for ($i = 2; $i < $n; $i++) {
        $fibonacci[] = $fibonacci[$i - 1] + $fibonacci[$i - 2];
    }

    return $fibonacci;
}

$input = 10;
echo ("Jika n = " . $input . " maka " . $input . " bilangan fibonacci pertama adalah ");
$output = fibonacciN($input);
$hasil = implode(",", $output);
echo $hasil;
