<?php
function numberSort($x)
{
    $y = count($x);
    for ($i = 0; $i < $y - 1; $i++) {
        for ($j = 0; $j < $y - $i - 1; $j++) {
            if ($x[$j] > $x[$j + 1]) {
                $temp = $x[$j];
                $x[$j] = $x[$j + 1];
                $x[$j + 1] = $temp;
            }
        }
    }
    return $x;
}

$input = array(1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8);
$hasilInput = implode(' ', $input);

$output = numberSort($input);
$hasilOutput = implode(' ', $output);

echo ("Input: " . $hasilInput . "\n");
echo ("Output: " . $hasilOutput);
