<?php
$candle = [3, 3, 9, 6, 7, 8, 23];
function lajuMelelehLilin($panjangLilin)
{
    $panjangLilinArr = explode(" ", $panjangLilin);
    $lajuLeleh = [1, 1, 2, 3, 5, 8, 13];

    while (!in_array(0, $panjangLilinArr)) {
        for ($i = 0; $i < count($panjangLilinArr); $i++) {
            $panjangLilinArr[$i] -= $lajuLeleh[$i];
        }
    }

    $pertamaMeleleh = array_search(0, $panjangLilinArr) + 1;

    return $pertamaMeleleh;
}

$input = "3 3 9 6 7 8 23";
$output = lajuMelelehLilin($input);

echo ("Lilin pertama yang meleleh adalah lilin ke-" . $output);
