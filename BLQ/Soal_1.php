<?php
function kombinasiItemTerbanyak($uang, $barang)
{
    $uangAwal = $uang;
    $jumlahBarang = 0;
    $uangKeluar = 0;
    $daftarBarang = array();
    $daftarHarga = array();

    for ($i = 0; $i < count($barang); $i++) {
        foreach ($barang[$i]['harga'] as $harga) {
            if ($harga <= $uang) {
                $daftarHarga[] = $harga;
                $daftarBarang[] = $barang[$i]['namaBarang'];
                $uang -= $harga;
                $uangKeluar += $harga;
            }
        }
    }

    $hasil = array_combine($daftarBarang, $daftarHarga);
    $jumlahBarang = count($daftarBarang);
    $barangSisa = $daftarBarang[count($daftarBarang) - 1];

    echo ("Uang Awal = " . $uangAwal . "\n");
    echo ("Uang yang dipakai = " . $uangKeluar . "\n");
    echo "Barang - barang yang bisa dibeli = $jumlahBarang (";
    foreach ($hasil as $key => $value) {
        echo "$key $value";
        if ($key != $barangSisa) {
            echo ", ";
        }
    }
    echo ")\n";
}

$uang = 1000;
$barang = array(
    array("namaBarang" => "kaca_mata", 'harga' => array(500, 600, 700, 800)),
    array("namaBarang" => "baju",      'harga' => array(200, 400, 350)),
    array("namaBarang" => "sepatu",    'harga' => array(400, 350, 200, 300)),
    array("namaBarang" => "buku",      'harga' => array(100, 50, 150))
);

kombinasiItemTerbanyak($uang, $barang);
