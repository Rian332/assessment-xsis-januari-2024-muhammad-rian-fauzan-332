<?php
function bewareTheManhole($jalur)
{
    $jalur = explode(" ", $jalur);
    $langkah = array();
    $stamina = 0;

    for ($i = 0; $i < count($jalur); $i++) {
        if (@$jalur[$i] == "O") {
            if ($stamina >= 2) {
                $stamina -= 2;
                for ($j = 0; $j < 3; $j++) {
                    $langkah[] = "J";
                    $i++;
                }
            } else {
                return  "FAILED";
            }
        } else {
            $stamina++;
            $langkah[] = "W";
        }
    }

    $hasil = "";
    foreach ($langkah as $step) {
        $hasil .= "$step ";
    }

    return $hasil;
}

$input = "_ _ _ _ _ O _ _ _";
$output = bewareTheManhole($input);

echo ("Langkah player = " . $output);
