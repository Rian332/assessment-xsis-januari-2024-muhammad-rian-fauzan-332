<?php
function donnaMinum($waktuOlahraga)
{
    $kaloriPerJam = array(
        30 => 9,
        20 => 13,
        50 => 15,
        80 => 17
    );

    $jam = $waktuOlahraga - 1;
    $kalori = array_search($jam, $kaloriPerJam);

    $menitOlahraga = 0.1 * $kalori * $jam;
    $airDiminum = 0;
    while ($menitOlahraga > 30) {
        $airDiminum += 100;
        $menitOlahraga -= 30;
    }

    $airDiminum += 500;
    $hasilDonna = array(
        "air" => $airDiminum,
        "menit" => $menitOlahraga
    );

    return $hasilDonna;
}

$input = 18;
$output = donnaMinum($input);
echo "Donna meminum air sebanyak " . $output['air'] . " cc dalam " . $output['menit'] . " menit berolahraga";
