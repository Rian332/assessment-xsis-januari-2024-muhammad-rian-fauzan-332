<?php
function minMax4Sum($deret)
{
    $deretArr = str_split($deret);
    sort($deretArr);

    $min = PHP_INT_MAX;
    $max = PHP_INT_MIN;

    for ($i = 0; $i <= (count($deretArr) - 4); $i++) {
        $sum = $deretArr[$i] + $deretArr[$i + 1] + $deretArr[$i + 2] + $deretArr[$i + 3];
        $min = min($min, $sum);
        $max = max($max, $sum);
    }

    $minMax = array(
        'jumlahMin' => $min,
        'jumlahMax' => $max
    );

    return $minMax;
}

$input = 1247869;
$output = minMax4Sum($input);
echo ("Nilai minimal = " . $output['jumlahMin'] . "\n");
echo ("Nilai maksimal = " . $output['jumlahMax']);
