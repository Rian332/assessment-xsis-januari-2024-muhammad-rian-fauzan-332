<?php
function ninjaHattori($string)
{
    $gunung = 0;
    $lembah = 0;
    $step = str_split($string);
    $count = 0;

    foreach ($step as $langkah) {
        if ($langkah === 'N') {
            $count++;
        } elseif ($langkah === 'T') {
            $count--;
        }

        if ($count === 0 && $langkah === 'N') {
            $lembah++;
        } elseif ($count === 0 && $langkah === 'T') {
            $gunung++;
        }
    }

    $hasilPerjalanan = array(
        'gunung' => $gunung,
        'lembah' => $lembah
    );

    return $hasilPerjalanan;
}

$input = "NNTNNNTTTTTNTTTNTN";
$output = ninjaHattori($input);

echo "Hattori melewati " . $output['gunung'] . " gunung" . "\n";
echo "Hattori melewati " . $output['lembah'] . " lembah";
