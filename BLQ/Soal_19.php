<?php
function pangrams($string)
{
    $lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
    $uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    $stringLow = strtolower($string);
    $stringUp = strtoupper($string);

    for ($i = 0; $i < strlen($lowercaseLetters); $i++) {
        $charLow = $lowercaseLetters[$i];
        $charUp = $uppercaseLetters[$i];

        if (strpos($stringLow, $charLow) === false && strpos($stringUp, $charUp) === false) {
            return "bukan pangram";
        }
    }
    return "pangram";
}

$input = "Sphinx of black quartz, judge my vow";
$input2 = "Brawny gods just flocked up to quiz and vex him";
$input3 = "Check back tomorrow; I will see if the book has arrived.";

$output = pangrams($input);
$output2 = pangrams($input2);
$output3 = pangrams($input3);

echo ("Kalimat " . $input . " adalah " . $output . "\n");
echo ("Kalimat " . $input2 . " adalah " . $output2 . "\n");
echo ("Kalimat " . $input3 . " adalah " . $output3);
