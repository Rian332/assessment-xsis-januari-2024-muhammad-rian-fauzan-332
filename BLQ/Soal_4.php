<?php
function primeNumbers($n)
{
    $angka = 0;
    $x = 0;
    $primeArr = array();

    while ($x != $n) {
        $temp = 0;
        for ($i = 1; $i <= $angka; $i++) {
            if ($angka % $i == 0) {
                $temp++;
            }
        }

        if ($temp == 2) {
            $primeArr[] = $angka;
            $x++;
        }
        $angka++;
    }

    for ($j = 0; $j < count($primeArr); $j++) {
        echo ($primeArr[$j] . " ");
    }
}

primeNumbers(10);
