<?php
$deretAngka = 8702717630713461643;

function statistik($deretAngka)
{
    $deretArr = str_split($deretAngka);
    sort($deretArr);

    // Mean
    $jumlah = array_sum($deretArr);
    $hasilMean = $jumlah / count($deretArr);

    //Median
    $midCount = 0;
    $hasilMedian = 0;
    if (count($deretArr) % 2 == 0) {
        $midCount = floor(count($deretArr) / 2);
        $hasilMedian = ($deretArr[$midCount] + $deretArr[$midCount + 1]) / 2;
    } else {
        $midCount = (count($deretArr) + 1) / 2;
        $hasilMedian = $deretArr[$midCount];
    }

    //Modus
    $frekuensiAngka = array_count_values($deretArr);
    $frekuensiTertinggi = max($frekuensiAngka);
    $hasilModus = array_search($frekuensiTertinggi, $frekuensiAngka);

    echo ("Mean = " . $hasilMean . "\n");
    echo ("Median = " . $hasilMedian . "\n");
    echo ("Modus = " . $hasilModus);
}

echo ("Deret Angka: " . $deretAngka . "\n");
statistik($deretAngka);
