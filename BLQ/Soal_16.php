<?php
$customer = 4;
$alergi = 1;

$makanan = [
    'Tuna Sandwich' => 42000,
    'Spaghetti Carbonara' => 50000,
    'Tea Pitcher' => 30000,
    'Pizza' => 70000,
    'Salad' => 30000
];

$menuIkan = 'Tuna Sandwich';

$pajak = array();
foreach ($makanan as $pajakMakanan => $harga) {
    $pajak[$pajakMakanan] = $harga + ($harga * 0.1) + ($harga * 0.05);
}

$pajakTanpaIkan = $pajak;
unset($pajakTanpaIkan[$menuIkan]);

foreach ($pajakTanpaIkan as $pajakMakanan => $harga) {
    $pajakTanpaIkan[$pajakMakanan] = $harga / 4;
}

$pajakDenganIkan = $pajak[$menuIkan] / 3;
$hargaTotal = array_sum($pajakTanpaIkan) + $pajakDenganIkan;

echo ($hargaTotal . " Rupiah");
