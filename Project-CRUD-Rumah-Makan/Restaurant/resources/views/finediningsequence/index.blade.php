@extends('index')

@section('title', 'Fine Dining Sequence')
@section('page-title', 'Fine Dining Sequence')
@section('content')

<div class="row">
    <div class="col">
        <div class="input-group w-75">
            <input id="searchKeyword" placeholder="Cari Nama" class="form-control fs-6">
            <button class="btn btn-primary m-0" onclick="find()"><i class="fas fa-search fa-lg"></i></button>
        </div>
    </div>
    <div class="col">
        <button onclick="addForm()" class="btn btn-primary mb-2" style="float:right;">Tambah</button>
    </div>
</div>
<table class="table table-primary">
    <thead>
        <tr>
            <th>Nama</th>
            <th class="text-center">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach($finediningseq as $name)
        <tr>
            <td>{{$name->name}}</td>
            <td colspan="2" class="text-center">
                <button class="btn btn-warning" onclick="editForm('{{$name->id}}')">Ubah</button>
                <button class="btn btn-danger" onclick="deleteForm('{{$name->id}}')">Hapus</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    function addForm() {
        $.ajax({
            url: 'http://localhost:8000/finediningsequence/addForm',
            method: 'GET',
            contentType: 'html',
            success: function(response) {
                $('#myModal').modal('show')
                $('.modal-header').show()
                $('.modal-title').html('Tambah Data Fine Dining Sequence')
                $('.modal-body').html(response)
                $('.modal-footer').hide()
            }
        });
    }

    function closeForm() {
        $('#myModal').modal('hide')
    }

    function editForm(id) {
        $.ajax({
            url: 'http://localhost:8000/finediningsequence/editForm/' + id,
            method: 'GET',
            contentType: 'html',
            success: function(response) {
                $('#myModal').modal('show')
                $('.modal-header').show()
                $('.modal-title').html('Ubah Data Fine Dining Sequence')
                $('.modal-body').html(response)
                $('.modal-footer').hide()
            }
        })
    }

    function deleteForm(id) {
        $.ajax({
            url: 'http://localhost:8000/finediningsequence/deleteForm/' + id,
            method: 'GET',
            contentType: 'html',
            success: function(response) {
                $('#myModal').modal('show')
                $('.modal-header').show()
                $('.modal-title').html('Hapus Data Fine Dining Sequence')
                $('.modal-body').html(response)
                $('.modal-footer').hide()
            }
        })
    }

    function find() {
        let name = $('#searchKeyword').val()
        console.log(name);
        if (name) {
            window.location.href = 'http://localhost:8000/finediningsequence/find/' + name
            return
        }
        window.location.href = 'http://localhost:8000/finediningsequence'
        return
    }
</script>


@endsection