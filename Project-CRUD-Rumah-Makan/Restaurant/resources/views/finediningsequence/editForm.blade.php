<form id="editFdsForm" method="POST" action="http://localhost:8000/api/finediningsequence/edit/{{$editFds->id}}">
    @csrf
    <div class="container-fluid px-3">
        <div class="row mt-0">
            <h4>Ubah Nama*</h4>
        </div>
        <div class="row mt-2">
            <input id="name" name="name" class="form-control" value="{{$editFds->name}}">
            <label class="text-danger" id="errorMessage"></label>
        </div>
        <div class="row mt-3">
            <div class="col text-end">
                <button class="btn btn-secondary w-75" type="button" data-bs-toggle="modal" onclick="closeForm()">Batal</button>
            </div>
            <div class="col">
                <button class="btn btn-warning w-75" type="button" onclick="checkData()">Ubah</button>
            </div>
        </div>
    </div>
</form>

<script>
    function checkData() {
        let name = $('#name').val()
        switch (true) {
            case name.length === 0:
                return $('#errorMessage').html('*Nama harus diisi');
                break;
            case name.trim() === "":
                return $('#errorMessage').html('*Nama hanya mengandung spasi');
                break;
        }
        $.ajax({
            url: 'http://localhost:8000/api/finediningsequence/check/' + name,
            method: 'GET',
            success: function(response) {
                if (response.data) {
                    $('#errorMessage').html('*Nama sudah ada')
                } else {
                    submitForm();
                }
            }
        })
        console.log(name);
    }

    function submitForm() {
        var form = document.getElementById("editFdsForm");
        form.submit();
    }
</script>