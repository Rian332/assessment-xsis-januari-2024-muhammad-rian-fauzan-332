<form id="deleteFdsForm" method="POST" action="http://localhost:8000/api/finediningsequence/delete/{{$deleteFds->id}}">
    @csrf
    <div class="container-fluid px-3">
        <div class="row mt-2 mb-4 text-center">
            <input name="user_id" id="user_id" hidden>
            <a class="fs-4">Anda akan menghapus <b>{{$deleteFds->name}}</b> !</a>
        </div>
        <div class="row mt-3">
            <div class="col text-end">
                <button class="btn btn-secondary w-75" type="button" data-bs-toggle="modal" onclick="closeForm()">Batal</button>
            </div>
            <div class="col">
                <button class="btn btn-danger w-75" type="submit">Hapus</button>
            </div>
        </div>
    </div>
</form>