<?php

use App\Http\Controllers\FineDiningSequenceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Fine Dining Sequence
Route::post('/finediningsequence/create', [FineDiningSequenceController::class, 'create']);
Route::get('/finediningsequence/check/{name}', [FineDiningSequenceController::class, 'checkData']);
Route::post('/finediningsequence/edit/{id}', [FineDiningSequenceController::class, 'edit']);
Route::post('/finediningsequence/delete/{id}', [FineDiningSequenceController::class, 'delete']);
