<?php

use App\Http\Controllers\FineDiningSequenceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('index');
});

//Fine Dining Sequence
Route::get('/finediningsequence', [FineDiningSequenceController::class, 'index']);
Route::get('/finediningsequence/addForm', [FineDiningSequenceController::class, 'addForm']);
Route::get('/finediningsequence/editForm/{id}', [FineDiningSequenceController::class, 'editForm']);
Route::get('/finediningsequence/deleteForm/{id}', [FineDiningSequenceController::class, 'deleteForm']);
Route::get('/finediningsequence/find/{name}', [FineDiningSequenceController::class, 'find']);
