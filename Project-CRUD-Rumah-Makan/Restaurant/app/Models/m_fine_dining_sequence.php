<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_fine_dining_sequence extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'm_fine_dining_sequences';
    protected $fillable = ['name', 'created_by', 'created_on'];
}
