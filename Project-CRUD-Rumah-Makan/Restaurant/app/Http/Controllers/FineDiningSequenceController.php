<?php

namespace App\Http\Controllers;

use App\Models\m_fine_dining_sequence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FineDiningSequenceController extends Controller
{
    public function index()
    {
        $fineDiningSeq = m_fine_dining_sequence::where('is_delete', false)->get();

        return view('finediningsequence.index', ['finediningseq' => $fineDiningSeq]);
    }

    public function addForm()
    {
        return view('finediningsequence.addForm');
    }

    public function checkData($name)
    {
        $exists = m_fine_dining_sequence::where('name', 'ILIKE', $name)->where('is_delete', false)->first();

        if ($exists) {
            return response()->json(['data' => true]);
        }

        return response()->json(['data' => false]);
    }

    public function create(Request $req)
    {
        $fineDiningSeq = m_fine_dining_sequence::where('name', 'ILIKE', $req->name)->where('is_delete', true)->first();

        if ($fineDiningSeq) {
            $fineDiningSeq->is_delete = false;
            $fineDiningSeq->name = $req->name;
            $fineDiningSeq->modified_by = 1;
            $fineDiningSeq->modified_on = now();
            $fineDiningSeq->update();
            return Redirect::back()->with('message', 'Data ditambahkan');
        } else {
            $fineDiningSeq = new m_fine_dining_sequence();
            $fineDiningSeq->name = $req->name;
            $fineDiningSeq->created_by = 1;
            $fineDiningSeq->created_on = now();
            $fineDiningSeq->save();
            return Redirect::back()->with('message', 'Data ditambahkan');
        }
    }

    public function editForm($id)
    {
        $fineDiningSeq = m_fine_dining_sequence::find($id);
        return view('finediningsequence.editForm', ['editFds' => $fineDiningSeq]);
    }

    public function edit(Request $req, $id)
    {
        $fineDiningSeq = m_fine_dining_sequence::findOrFail($id);

        if ($fineDiningSeq) {
            $fineDiningSeq->name = $req->name;
            $fineDiningSeq->modified_by = 1;
            $fineDiningSeq->modified_on = now();
            $fineDiningSeq->update();

            return Redirect::back();
        } else {
            return response()->json(['message' => "data tidak ditemukan"], 200);
        }
    }

    public function deleteForm($id)
    {
        $fineDiningSeq = m_fine_dining_sequence::where('id', $id)->first();

        return view('finediningsequence.deleteForm', ['deleteFds' => $fineDiningSeq]);
    }

    public function delete($id)
    {
        $fineDiningSeq = m_fine_dining_sequence::find($id);

        if ($fineDiningSeq) {
            $fineDiningSeq->is_delete = true;
            $fineDiningSeq->deleted_by = 1;
            $fineDiningSeq->deleted_on = now();
            $fineDiningSeq->update();

            return Redirect::back()->with('message', 'Data Dihapus');
        } else {
            return response()->json(['message' => 'data tidak ditemukan'], 200);
        }
    }

    public function find($name)
    {
        $fineDiningSeq = m_fine_dining_sequence::where('name', 'ILIKE', '%' . $name . '%')->where('is_delete', false)->get();

        return view('finediningsequence.index', ['finediningseq' => $fineDiningSeq]);
    }
}
